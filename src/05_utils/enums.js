const EnumPreferredUnit = {
    METRIC: 'metric',
    IMPERIAL: 'imperial'
};

export {
    EnumPreferredUnit
}
