import { EnumPreferredUnit } from '@/05_utils/enums';

function celsiusToFahrenheitConvertedFormat (preferredUnit, temp) {
    if (preferredUnit === EnumPreferredUnit.METRIC) {
        return temp + ' °C';
    } else {
        const _fahrenheitValue = (temp * 9/5) + 32;
        return _fahrenheitValue.toFixed(2) + ' °F';
    }
}

export {
    celsiusToFahrenheitConvertedFormat
}
