import Vue from 'vue';
import VueRouter from 'vue-router';
import Dashboard from '../03_pages/dashboard.vue';
import City from '../03_pages/city.vue';

// global state
import store from '../04_vuex/index';
Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        name: 'Dashboard',
        component: Dashboard
    },
    {
        path: '/city/:cityName?',
        name: 'City',
        component: City,
        beforeEnter: (to, from, next) => {
            const selectedCityInfo = store.getters.selectedCityInfo;
            // only route to city detail page, if it is there already selected city information
            if (selectedCityInfo) {
                next();
            } else {
                next('');
            }
        }
    }
];

const router = new VueRouter({
    routes
});

export default router;
