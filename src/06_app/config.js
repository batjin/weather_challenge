import { EnumPreferredUnit } from '../05_utils/enums';

export default {
    WEATHER_API_KEY: '', // please use your API key
    WEATHER_API_URL_SINGLE_CITY: 'https://api.openweathermap.org/data/2.5/weather',
    WEATHER_API_URL_GROUP_CITIES: 'https://api.openweathermap.org/data/2.5/group',
    OPEN_WEATHER_ID_BERLIN: '2950159',
    OPEN_WEATHER_ID_LONDON: '2643743',
    WEATHER_DEFAULT_UNIT: EnumPreferredUnit.METRIC
}
