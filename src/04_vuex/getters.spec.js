import getters from './getters'

const state = {
    isAPICallingOnMainGroupCitesWeather: false,
    isAPICALLingOnMyLocationWeather: true,
    mainGroupCitesWeather: 'test',
    myLocationWeatherInfo: 'local',
    selectedCityInfo: 'city',
    preferredUnit: 'metric',
    myLocationName: 'name',
    isModalOpened: false
};

describe('vuex/getters.js - ', () => {
    it('isAPICallingOnMainGroupCitesWeather', () => {
        const result = getters.isAPICallingOnMainGroupCitesWeather(state);
        expect(result).toEqual(false);
    });

    it('isAPICALLingOnMyLocationWeather', () => {
        const result = getters.isAPICALLingOnMyLocationWeather(state);
        expect(result).toEqual(true);
    });

    it('mainGroupCitesWeather', () => {
        const result = getters.mainGroupCitesWeather(state);
        expect(result).toEqual('test');
    });

    it('myLocationWeatherInfo', () => {
        const result = getters.myLocationWeatherInfo(state);
        expect(result).toEqual('local');
    });

    it('selectedCityInfo', () => {
        const result = getters.selectedCityInfo(state);
        expect(result).toEqual('city');
    });

    it('preferredUnit', () => {
        const result = getters.preferredUnit(state);
        expect(result).toEqual('metric');
    });

    it('myLocationName', () => {
        const result = getters.myLocationName(state);
        expect(result).toEqual('name');
    });

    it('isModalOpened', () => {
        const result = getters.isModalOpened(state);
        expect(result).toEqual(false);
    });
})
