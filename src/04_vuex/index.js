import Vue from 'vue';
import Vuex from 'vuex';
import AppConfig from '../06_app/config';

// Modules
import Actions from './actions';
import Mutations from './mutations';
import Getters from './getters';

const _preferredUnitFromLocalstorage = localStorage.getItem('preferredUnit');
const _myLocationNameFromLocalstorage = localStorage.getItem('myLocationName');

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        /**
         * This flag is used to display the full page loading animation during the main cities API call is ongoing
         */
        isAPICallingOnMainGroupCitesWeather: false,
        isAPICALLingOnMyLocationWeather: false,
        /**
         * mainGroupCitesWeather: includes array of the main 2 cities weather information
         */
        mainGroupCitesWeather: [],
        /**
         * myLocationWeatherInfo: includes user's typed locations weather information
         */
        myLocationWeatherInfo: null,
        /**
         * selectedCityInfo: is used for city detailed information on city detail page
         */
        selectedCityInfo: null,
        /**
         * preferredUnit: can be selected by use, if not its the default from config
         */
        preferredUnit: _preferredUnitFromLocalstorage ? _preferredUnitFromLocalstorage : AppConfig.WEATHER_DEFAULT_UNIT,
        myLocationName: _myLocationNameFromLocalstorage ? _myLocationNameFromLocalstorage : '',
        /**
         * isModalOpened: controlling the visibility of modal
         */
        isModalOpened: false
    },
    mutations: {
        ... Mutations
    },
    actions: {
        ... Actions
    },
    getters: {
        ... Getters
    }
});

