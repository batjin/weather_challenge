export default {
    SET_API_FLAG_ON_MAIN_GROUP_CITIES_WEATHER (state, flag) {
        state.isAPICallingOnMainGroupCitesWeather = flag;
    },
    SET_API_FLAG_ON_MY_LOCATION_WEATHER (state, flag) {
        state.isAPICALLingOnMyLocationWeather = flag;
    },
    SET_MAIN_GROUP_CITIES_WEATHER (state, list) {
        state.mainGroupCitesWeather = list;
    },
    SET_MY_LOCATION_WEATHER_INFO (state, info) {
        state.myLocationWeatherInfo = info;
    },
    SET_SELECTED_CITY_WEATHER_INFO (state, info) {
        state.selectedCityInfo = info;
    },
    SET_MODAL_VISIBILITY (state, isVisible) {
        state.isModalOpened = isVisible;
    },
    SET_PREFERRED_UNIT (state, unit) {
        state.preferredUnit = unit;
    },
    SET_MY_LOCATION_NAME (state, name) {
        state.myLocationName = name;
    }
}
