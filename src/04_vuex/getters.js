export default {
    isAPICallingOnMainGroupCitesWeather: state => state.isAPICallingOnMainGroupCitesWeather,
    isAPICALLingOnMyLocationWeather: state => state.isAPICALLingOnMyLocationWeather,
    mainGroupCitesWeather: state => state.mainGroupCitesWeather,
    myLocationWeatherInfo: state => state.myLocationWeatherInfo,
    selectedCityInfo: state => state.selectedCityInfo,
    preferredUnit: state => state.preferredUnit,
    myLocationName: state => state.myLocationName,
    isModalOpened: state => state.isModalOpened
}
