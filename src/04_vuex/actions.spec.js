import Actions from './actions';

describe('vuex/actions.js', () => {
    let commit;
    let dispatch;

    beforeEach(() => {
        commit = jest.fn();
        dispatch = jest.fn((action = '', resolves = true) => new Promise((resolve, reject) => {
            if (resolves) {
                resolve();
            } else {
                reject();
            }
        }));
    });

    it('setSelectedCity action', () => {
        Actions.setSelectedCity({ commit }, 'test');
        expect(commit).toHaveBeenCalledWith('SET_SELECTED_CITY_WEATHER_INFO', 'test');
    });

    it('setModalVisibility action', () => {
        Actions.setModalVisibility({ commit }, 'test');
        expect(commit).toHaveBeenCalledWith('SET_MODAL_VISIBILITY', 'test');
    });

    it('setPreferredUnit action', () => {
        Actions.setPreferredUnit({ commit }, 'test');
        expect(commit).toHaveBeenCalledWith('SET_PREFERRED_UNIT', 'test');
    });

    it('setMyLocationName action', () => {
        Actions.setMyLocationName({ commit }, 'test');
        expect(commit).toHaveBeenCalledWith('SET_MY_LOCATION_NAME', 'test');
    });

    it('storeMainCitiesWeatherInformation action', () => {
        Actions.storeMainCitiesWeatherInformation({ dispatch, commit });
        expect(dispatch).toHaveBeenCalledWith('fetchMainCitiesWeather');
    });
})
