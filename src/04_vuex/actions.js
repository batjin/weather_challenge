import Vue from 'vue';
import AppConfig from '../06_app/config';

export default {
    storeMainCitiesWeatherInformation ({ dispatch, commit }) {
        return new Promise((resolve, reject) => {
            dispatch('fetchMainCitiesWeather')
                .then((list) => {
                    commit('SET_MAIN_GROUP_CITIES_WEATHER', list);
                    resolve(list);
                })
                .catch((error) => {
                    console.error('[store/storeMainCitiesWeatherInformation] error fetching data.', error);
                    reject(error);
                });
        });
    },
    fetchMainCitiesWeather ({ commit }) {
        return new Promise((resolve, reject) => {
            commit('SET_API_FLAG_ON_MAIN_GROUP_CITIES_WEATHER', true);
            Vue.http.get(AppConfig.WEATHER_API_URL_GROUP_CITIES,
                {
                    params: {
                        id: AppConfig.OPEN_WEATHER_ID_LONDON + ',' + AppConfig.OPEN_WEATHER_ID_BERLIN,
                        appid: AppConfig.WEATHER_API_KEY,
                        units: AppConfig.WEATHER_DEFAULT_UNIT
                    }
                })
                .then(response => {
                    // API was successfully
                    console.info('[store/fetchMainCitiesWeather] fetched data', response);
                    commit('SET_API_FLAG_ON_MAIN_GROUP_CITIES_WEATHER', false);
                    if (response && response.body && response.body.list) {
                        resolve(response.body.list);
                    } else {
                        reject(response);
                    }
                }, error => {
                    // API was failed
                    console.error('[store/fetchMainCitiesWeather] error fetching data.', error);
                    commit('SET_API_FLAG_ON_MAIN_GROUP_CITIES_WEATHER', false);
                    reject(error);
                })
        });
    },
    storeMyLocationWeatherInformation ({ dispatch, commit }) {
        return new Promise((resolve, reject) => {
            dispatch('fetchMyLocationWeather')
                .then((body) => {
                    commit('SET_MY_LOCATION_WEATHER_INFO', body);
                    resolve(body);
                })
                .catch((error) => {
                    console.error('[store/storeMyLocationWeatherInformation] error fetching data.', error);
                    reject(error);
                });
        });
    },
    fetchMyLocationWeather ({ commit, getters }) {
        return new Promise((resolve, reject) => {
            commit('SET_API_FLAG_ON_MY_LOCATION_WEATHER', true);
            Vue.http.get(AppConfig.WEATHER_API_URL_SINGLE_CITY,
                {
                    params: {
                        q: getters.myLocationName,
                        appid: AppConfig.WEATHER_API_KEY,
                        units: AppConfig.WEATHER_DEFAULT_UNIT
                    }
                })
                .then(response => {
                    // API was successfully
                    console.info('[store/fetchMyLocationWeather] fetched data', response);
                    commit('SET_API_FLAG_ON_MY_LOCATION_WEATHER', false);
                    if (response && response.body) {
                        // update localstorage with user's typed location name
                        localStorage.setItem('myLocationName', getters.myLocationName);
                        resolve(response.body);
                    } else {
                        reject(response);
                    }
                }, error => {
                    // API was failed
                    console.error('[store/fetchMyLocationWeather] error fetching data.', error);
                    commit('SET_API_FLAG_ON_MY_LOCATION_WEATHER', false);
                    reject(error);
                })
        });
    },
    setSelectedCity ({ commit }, cityWeatherInfo) {
        commit('SET_SELECTED_CITY_WEATHER_INFO', cityWeatherInfo);
    },
    setModalVisibility ({ commit }, isVisible) {
        commit('SET_MODAL_VISIBILITY', isVisible);
    },
    setPreferredUnit ({ commit }, unit) {
        commit('SET_PREFERRED_UNIT', unit);
        // update localstorage with user's preferred unit
        localStorage.setItem('preferredUnit', unit);
    },
    setMyLocationName ({ commit }, name) {
        commit('SET_MY_LOCATION_NAME', name);
    }
}
