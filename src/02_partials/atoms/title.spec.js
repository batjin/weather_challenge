import { createLocalVue, mount } from '@vue/test-utils';
import Title from './title.vue';

const localVue = createLocalVue();

describe('title.vue', () => {
    let wrapper;

    beforeEach(() => {
        wrapper = mount(Title, {
            localVue,
            propsData: {
                title: 'Dashboard'
            }
        });
    });

    it('title.vue - test if title is loaded properly', () => {
        expect(wrapper.find('.title').text()).toBe('Dashboard');
    });
});
