import Vue from 'vue';
import App from './06_app/app.vue';
import router from './06_app/router';
import store from './04_vuex';

import VueResource from 'vue-resource';
import Buefy from 'buefy';
import 'buefy/dist/buefy.css'

Vue.use(VueResource);
Vue.use(Buefy);

Vue.config.productionTip = false;

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app');
