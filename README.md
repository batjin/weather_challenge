# weather_app_challenge
This weather app challenge is written in VueJS and Buefy. Buefy is used to get quickly some components.
The main functionality is already implemented. But there are some TODO:
- increase test coverage
- using i18n to add translations
- change of displaying weather on dashboard. Currently they are not the same. Because my location has some other icons and functionality.
- break some components from dashboard into own components. Specially the one for user typed location and the handling
- git commits wasn't perfectly splitted into commits. Because of my main work, i hadn't chance to cleanly splitted solutions and features.

## Before start project
If you cloning it from Bitbucket, then OpenWeatherMap API key is not included in it. Please replace with your own key the variable "WEATHER_API_KEY" on the following configuration files.
```
06_app/config.js
```


## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Running jest for testing
```
npm run test
```

Icons provided by
<a href="https://icons8.com/icon/64759/circled-right">Circled Right icon by Icons8</a>
